# DoxygenEpitech is a simple way to add Doxygen documentation to your Epitech projects.

- [Features](#features)
- [Requirements](#requirements)
- [Communication](#communication)
- [License](#license)

<h2 id="features">
Features
</h2>

- Fully compliant with the Epitech/Unix coding style.
- Simple and intuitive interface.

<h2 id="requirements">
Requirements
</h2>

- Latest version of Visual Studio Code.

<h2 id="communication">
Communication
</h2>

- If you **need help**, please reach me at charles.aubert@epitech.eu.

<h2 id="license">
License
</h2>

DoxygenEpitech is released under the **MIT license**.