# Changelog

## [1.0.2] - 2019-01-19
### Fixed
- Fixed an issue preventing the documentation of structures.

## [1.0.1] - 2019-01-11
### Added
- Extension icon.

### Changed
- Updated the README.md