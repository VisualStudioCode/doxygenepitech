// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

class Tools {
	constructor() {
	}

	insertTextAtPos(editor, text, pos) {
		var document = null;
		var edit = null;

		if (editor == null) {
			vscode.window.showErrorMessage("Please select a file first.");
			return (2);
		}
		document = editor.document;
		edit = new vscode.WorkspaceEdit();
		edit.set(document.uri, [vscode.TextEdit.insert(new vscode.Position(pos.line, pos.character), text)]);
		vscode.workspace.applyEdit(edit);
		return (0);
	}

	analyseParamType(type) {
		var type = String(type).toLowerCase();

		if (type == "in" || type == "out" || type == "in,out")
			return (type);
		return ("");
	}

	checkInputValidity(input) {
		if (input == undefined) {
			vscode.window.showInformationMessage("No comment inserted.");
			return (2);
		} else if (input == "") {
			return (1);
		}
		return (0);
	}

	async userInput(prompt, placeHolder) {
		var result = await vscode.window.showInputBox({
			ignoreFocusOut: true,
			prompt: prompt,
			placeHolder: placeHolder
		});

		return (result);
	}
}

class CommentInserter {
	constructor(tool) {
		this.tool = tool;
		this.commands = {
			"init": this.insertHeader,
			"function": this.insertFunction,
			"struct": this.insertGeneric,
			"enum": this.insertGeneric,
			"member": this.insertMember,
			"class": this.insertGeneric,
			"namespace": this.insertGeneric,
			"union": this.insertGeneric
		}
	}

	updateEditor() {
		this.editor = vscode.window.activeTextEditor;
		if (!this.editor) {
			vscode.window.showErrorMessage("Please select a file first.");
			return (2);
		}
		if (this.editor.selection.isEmpty) {
			this.selection = this.editor.selection.active
		}
		return (0);
	}

	async insertFunction(input) {
		console.log("Inserting a function comment...");
		var text = "/**\n* \\brief";
		var description = await this.tool.userInput("Function brief description:", "");

		if (this.tool.checkInputValidity(description) != 0) {
			return (0);
		}
		text += " " + description + "\n";
		for (var i = 0; i < 4; i++) {
			var fullparam = "* \\param";
			var type = await this.tool.userInput("Param " + (i + 1) + " type: 'in' - 'out' - 'in,out'", "Empty if no parameter.");
			if (this.tool.checkInputValidity(type) == 2) {
				return (0);
			} else if (this.tool.checkInputValidity(type) == 1) {
				break;
			}
			fullparam += "[" + this.tool.analyseParamType(type) + "]";
			var param = await this.tool.userInput("Param " + (i + 1) + " description:", "");
			if (this.tool.checkInputValidity(param) != 0) {
				return (0);
			}
			fullparam += " " + param + "\n";
			text += fullparam;
		}
		var ret = await this.tool.userInput("Return value description:", "Empty if void.");
		if (this.tool.checkInputValidity(ret) == 2)
			return (0);
		else if (this.tool.checkInputValidity(ret) == 0) {
			text += "* \\return " + ret + "\n";
		}
		text += "*/";
		this.tool.insertTextAtPos(this.editor, text, {
			"line": this.selection.line,
			"character": 0
		});
		return (0);
	}

	async insertHeader(input) {
		console.log("Inserting a file header...");
		var description = await this.tool.userInput("File brief description:", "");

		if (this.tool.checkInputValidity(description) != 0) {
			return (0);
		}
		this.tool.insertTextAtPos(this.editor, "/**\n* \\file\n* \\brief " + description + "\n*/\n", {
			"line": 6,
			"character": 0
		});
		return (0);
	}

	async insertGeneric(input) {
		console.log("Inserting a " + input + " name comment...");
		var name = null;
		var description = null;
		var text = "/**\n";

		name = await this.tool.userInput(input + " name:", "");
		if (this.tool.checkInputValidity(name) != 0) {
			return (0);
		}
		text += "* \\" + input + " " + name + "\n";
		description = await this.tool.userInput("Brief " + input + " description:", "");
		if (this.tool.checkInputValidity(description) != 0) {
			return (0);
		}
		text += "* \\brief " + description + "\n";
		text += "*/";
		this.tool.insertTextAtPos(this.editor, text, {
			"line": this.selection.line,
			"character": 0
		});
		return (0);
	}

	async insertMember(input) {
		console.log("Inserting a member description...");
		var description = null;
		var text = "";

		description = await this.tool.userInput("Member description:", "");
		if (this.tool.checkInputValidity(description) != 0) {
			return (0);
		}
		text += "/*!< " + description + " */";
		this.tool.insertTextAtPos(this.editor, text, {
			"line": this.selection.line,
			"character": this.selection.character
		});
		return (0);
	}
}

class CommentSelecter {
	constructor(tool, inserter) {
		this.inserter = inserter;
		this.editor = null;
		this.line = 0;
		this.tool = tool;
		this.character = 0;
		this.commandList = [
			{
				"description": "insert a doxygen header for this file.",
				"label": "Init"
			}, {
				"description": "insert a comment block for a function.",
				"label": "Function"
			}, {
				"description": "insert a comment block for a structure.",
				"label": "Struct"
			}, {
				"description": "insert a comment block for an enum.",
				"label": "Enum"
			}, {
				"description": "insert an inline comment for a enum/structure member.",
				"label": "Member"
			}, {
				"description": "insert a comment block for a class.",
				"label": "Class"
			}, {
				"description": "insert a comment block for a namespace.",
				"label": "Namespace"
			}, {
				"description": "insert a comment block for a union.",
				"label": "Union"
			}, {
				"description": "leave the extension.",
				"label": "Quit"
			}
		]
	}

	commentSelection() {
		vscode.window.showQuickPick(this.commandList, {
			ignoreFocusOut: true,
			placeHolder: "Type 'help' for a list of commands."
		}).then(item => {
			if (this.inserter.updateEditor() != 0)
				return (0);
			if (item == undefined) {
				vscode.window.showInformationMessage("See you!");
				return (0);
			}
			var command = item.label;
			command = String(command).toLowerCase();
			if (command == "quit") {
				vscode.window.showInformationMessage("See you!");
				return (0);
			}
			if (this.inserter.commands.hasOwnProperty(command)) {
				console.log(command);
				this.inserter.commands[command].call(this.inserter, command).then((err) => {
					if (err) {
						vscode.window.showErrorMessage("Request error.");
						console.log(err);
						return (0);
					}
					return (this.commentSelection());
				});
			} else {
				vscode.window.showErrorMessage("Unkown command, please type \"help\".");
			}
		});
		return (0);
	}
}

// Entry point of the extension. Called when the startup command is executed.
function activate(context) {
	console.log("Extension is active.");
	let tool = new Tools();
	let inserter = new CommentInserter(tool);
	let selecter = new CommentSelecter(tool, inserter);
	let disposable = vscode.commands.registerCommand('extension.startUpMessage', function () {
		vscode.window.showInformationMessage("Hi!");
		if (selecter.commentSelection() != 0)
			return (0);
	});

	context.subscriptions.push(disposable);
	return (0);
}

exports.activate = activate;

// Leaving point of the extension.
function deactivate() {
	console.log("Extension is shutting down...");
}

exports.deactivate = deactivate;